# Magnussen Opgaver 3
---

A lightweight task management system utilising txt files (and a notepad-like program) that are managed and accessed from the command line.

## ENTITIES
---

Magop3 has three different entity types.

#### Ticket
A task to be completed, each has a corresponding text file.

Tickets have the following attributes:
- `ticket_id`: A unique ID to reference the ticket with.
- `title`: The title of the ticket.

#### Project
Each task is assigned to an over-arching project.

Projects have the following attributes:
- `project_code`: A three-letter code that serves as the project unique ID (and is also used to reference the project in commands).
- `title`: The project title.

#### Resolution
Each task is also assigned a resolution, this summarises:
- The current status of each ticket. In Progress and Complete are two of the resolutions.
- The priority of each ticket. There are several resolutions that describe a time period in which the ticket is likely to become In Progress.

Resolutions have the following attributes:
- `resolution_code`: A three-letter code that serves as the resolution unique ID (and is also used to reference the resolution in commands).
- `title`: The resolution title.

Unlike the tickets and projects, the resolutions are "set in stone" so to speak (for now at least...).

## ACTIONS
---

There are five actions that can be carried out on the three entities.

#### New
*Available for: ticket, project*

Create a new ticket or project.

Example: `magop3 new ticket "Test Ticket" prj tdy`<br>
Create a new ticket called "Test Ticket", assigning it to project with `project_code` "prj" and resolution with `resolution_code` "tdy" (Today).

#### Alter
*Available for: ticket, project*

Alter the entity in some way (currently only available alteration is to alter the title).

Example: `magop3 alter project prj --title "Project Vulcan"`<br>
Alter the project with `project_code` "prj" to have `title` "Project Vulcan".

#### Assign
*Available for: ticket*

Assign the ticket to a project or resolution.

Example: `magop3 assign ticket 25 -r cmp`<br>
Assign the ticket with `ticket_id` 25 to resolution with `resolution_code` "cmp" (Complete).

#### Load
*Available for: ticket*

Load the txt file for a ticket.

Example: `magop3 load ticket 101`<br>
Load the text file for the ticket with `ticket_id` 101.

#### Print
*Available for: ticket, project, resolution*

Print to the terminal a summary of individual versions of each entity with filtering options.

Example: `magop3 print ticket -p prj -r inp`<br>
Print the tickets that are assigned to project with `project_code` "prj" and assigned to resolution with `resolution_code` "inp" (In Progress).

## SETUP
-------

#### Environment Variables

magop3 uses environment variables to hold various "settings". These are kept in a `.bash_profile` file in the virtual environment and sourced by the single shell script to run magop3.

This file must be in the venv and export the following variables:
- DB_HOST, DB_DATABASE, DB_USER, DB_PASSWORD: The usual arguments required to configure a database connection.
- VAULTS: The filepath to the **vaults** directory where the text files will be kept (see below).
- NOTEPAD: The name of the notepad program to be used by magop3 (I use "notepadqq") to be used in conjuction with `subprocess.Popen`.
- PYTHONPATH: Add the magop3 home directory to the PYTHONPATH.

#### Database
magop3 uses a database to store the details of the tickets, projects and resolutions - and the ticket project and resolution assignments.

To function, magop3 requires the creation of this database (recommended name `magop3`). Once a database exists, `psql -d magop3 -f sql_migrations/migrate.sql` will create the structure the database requires.

#### Txt Files
The .txt files themselves are to be stored in a directory called **vaults** that needs to sit in the home directory. This can be created by `mkdir vaults`.

#### Alias
A setup I choose to use is to introduce a command line alias into the .bash_aliases file as follows:
`alias magop3=". ~/magop3/bin/run.sh"

This allows magop3 to be utilised from any directory on the machine.

