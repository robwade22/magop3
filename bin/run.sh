#!/bin/bash

source ~/magop3/.env/bin/activate
source ~/magop3/.env/.bash_profile

python3 -m magop3 "$@"

deactivate
