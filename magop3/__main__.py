"""
The main "interface" for magop3.

Receive, parse and action commands through the appropriate Handler.
"""

import argparse

from magop3.handler import ProjectHandler, ResolutionHandler, TicketHandler

ENTITY_HANDLERS = {
    'ticket': TicketHandler,
    'project': ProjectHandler,
    'resolution': ResolutionHandler
}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    action_parsers = parser.add_subparsers(dest='action')

    new_parser = action_parsers.add_parser('new')
    print_parser = action_parsers.add_parser('print')
    assign_parser = action_parsers.add_parser('assign')
    alter_parser = action_parsers.add_parser('alter')
    load_parser = action_parsers.add_parser('load')
    delete_parser = action_parsers.add_parser('delete')

    # NEW
    new_subparsers = new_parser.add_subparsers(dest='entity')

    # --- ticket
    new_ticket_parser = new_subparsers.add_parser('ticket')
    new_ticket_parser.add_argument('title', type=str)
    new_ticket_parser.add_argument('project_code', type=str)
    new_ticket_parser.add_argument('resolution_code', type=str)
    new_ticket_parser.add_argument('--load', action='store_true', default=True)

    # --- project
    new_project_parser = new_subparsers.add_parser('project')
    new_project_parser.add_argument('title', type=str)
    new_project_parser.add_argument('project_code', type=str,
        help="A three-letter code for quick referencing.")

    # PRINT
    print_subparsers = print_parser.add_subparsers(dest='entity')

    # --- ticket
    print_ticket_parser = print_subparsers.add_parser('ticket')
    print_ticket_parser.add_argument('-p', '--project_code', nargs='+', type=str)
    print_ticket_parser.add_argument('-r', '--resolution_code', nargs='+', type=str)

    # --- resolution, project
    print_subparsers.add_parser('resolution')
    print_subparsers.add_parser('project')

    # ASSIGN
    assign_subparsers = assign_parser.add_subparsers(dest='entity')

    # --- ticket
    assign_ticket_parser = assign_subparsers.add_parser('ticket')
    assign_ticket_parser.add_argument('ticket_id', type=int)
    assign_ticket_parser.add_argument('-p', '--project_code', type=str)
    assign_ticket_parser.add_argument('-r', '--resolution_code', type=str)
    assign_ticket_parser.add_argument('--load', action='store_true', default=False)

    # ALTER
    alter_subparsers = alter_parser.add_subparsers(dest='entity')

    # --- ticket
    alter_ticket_parser = alter_subparsers.add_parser('ticket')
    alter_ticket_parser.add_argument('ticket_id', type=int)
    alter_ticket_parser.add_argument('--title', type=str)
    alter_ticket_parser.add_argument('--load', action='store_true', default=False)

    # --- project
    alter_project_parser = alter_subparsers.add_parser('project')
    alter_project_parser.add_argument('project_code', type=str)
    alter_project_parser.add_argument('--title', type=str)

    # LOAD
    load_subparsers = load_parser.add_subparsers(dest='entity')

    # --- ticket
    load_ticket_parser = load_subparsers.add_parser('ticket')
    load_ticket_parser.add_argument('ticket_id', nargs='+', type=int)

    # DELETE
    delete_subparsers = delete_parser.add_subparsers(dest='entity')

    # --- ticket
    delete_ticket_parser = delete_subparsers.add_parser('ticket')
    delete_ticket_parser.add_argument('-i', '--ticket_id', type=int)
    delete_ticket_parser.add_argument('-p', '--project_code', type=str)
    delete_ticket_parser.add_argument('-r', '--resolution_code', type=str)

    # --- project
    delete_project_parser = delete_subparsers.add_parser('project')
    delete_project_parser.add_argument('project_code', type=str)

    args = parser.parse_args()

    valid_statement = (args.action is not None) and (args.entity is not None)
    if not valid_statement:
        print('Invalid statement - must pass at least an action and an entity')
    else:
        # Select the corresponding entity handler for this entity then call the
        # method that corresponds to the action.
        handler = ENTITY_HANDLERS[args.entity]

        # Didn't want to name methods "print()" but want to be able to pass
        # "print" from the command line.
        action = 'print_' if args.action == 'print' else args.action
        func = getattr(handler, action)
        func(**vars(args))
