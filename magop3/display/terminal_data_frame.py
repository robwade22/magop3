import pandas as pd

MAX_COL_WIDTH = 35
RESOLUTION_SORT_IX = {
    'In Progress': 0, 
    'Today': 1,
    'This Week': 2,
    'Next Week': 3,
    'Next Month': 4,
    'Next Quarter': 5,
    'Complete': 6,
    'Decommissioned': 7
}


class TerminalDataFrame:
    def __init__(self, df: pd.DataFrame):
        self.df = df
        self.column_df = None
        self.seperator = None

    def establish_column_df(self):
        """Create a DataFrame holding information about the columns."""
        data_char_max = self.df.apply(lambda x: x.apply(len).max(), axis=0)
        header_char_max = pd.Series(
            index=self.df.columns, data=self.df.columns.str.len()
        )

        self.column_df = pd.concat([data_char_max, header_char_max], axis=1)
        self.column_df['column_width'] = self.column_df.max(axis=1)

        # In fact we want to limit column_width - this value is such that
        # the table can be printed fine in the standard half-screen terminal.
        self.column_df['column_width'] = self.column_df.column_width.apply(
            lambda x: min(x, MAX_COL_WIDTH)
        )
    
    def sort_by_resolution(self):
        """Sort the DataFrame based on the resolution of each row, moving the
        most "urgent" to the top."""
        self.df['resolution_ix'] = self.df.resolution.map(RESOLUTION_SORT_IX)
        self.df.sort_values('resolution_ix', inplace=True)
        self.df.drop(['resolution_ix'], axis=1, inplace=True)

    def format_cell(self, string, column_name, center=False):
        column_width = self.column_df.loc[column_name, 'column_width']

        if center:
            formatted = string.center(column_width)
        else:
            if len(string) > MAX_COL_WIDTH:
                string = string[:(MAX_COL_WIDTH - 3)] + '...'
            formatted = string.ljust(column_width)
        return formatted
    
    def print_bar(self):
        full_column_width = (
            self.column_df.column_width.sum() + 
            len(self.seperator)*(len(self.column_df) - 1)
        )
        print('-'*full_column_width)

    def print_row(self, row):
        row_cells = [self.format_cell(value, col)
                     for col, value in row.items()]
        print(self.seperator.join(row_cells)) 

    def print_header(self):
        header_cells = [self.format_cell(col, col, center=True) 
                        for col in self.df.columns]
        print(self.seperator.join(header_cells))
    
    def publish(self, seperator=None):
        self.seperator = seperator if seperator else ' | '
        self.establish_column_df()
        try:
            self.sort_by_resolution()
        except AttributeError:
            # No resolution to sort by.
            pass

        print('\n')
        if self.df.empty:
            print('No records found!')
        else:
            self.print_header()
            self.print_bar()
            self.df.apply(self.print_row, axis=1)
        print('\n')
