import os
from subprocess import Popen

from magop3.db import DB


class NoTicketFound(Exception):
    """
    Trying to load a ticket with a ticket_id that is not assigned.
    """


class TicketBuilder:
    keys = ['Ticket ID:', 'Title:', 'Project:', 'Resolution:', 'Created:']

    def __init__(self, ticket_id):
        self.ticket_id = ticket_id
        self.ticket_filename = f'T{self.ticket_id}.txt'
        self.ticket_filepath = os.path.join(
            os.environ['VAULTS'], self.ticket_filename
        )

        self.ensure_filepath()

    def ensure_filepath(self):
        if not os.path.exists(self.ticket_filepath):
            open(self.ticket_filepath, 'w').close()

    def fetch_existing_ticket_body(self):
        """Fetch the rows currently in the txt file."""
        with open(self.ticket_filepath) as txtfile:
            ticket_rows = txtfile.read().split('\n')

        try:
            bar_index = ticket_rows.index('-'*10)
            body_rows = ticket_rows[bar_index + 1:]
        except (IndexError, ValueError):
            # Want two empty rows in place of the body.
            body_rows = ['', '']
        return body_rows

    @staticmethod
    def assignment_statement(title, timestamp_assigned=None):
        statement = f'{title}'
        if timestamp_assigned:
            statement += f' (since {timestamp_assigned.date().isoformat()})'
        return statement

    def create_header(self):
        """
        Gather the data for the header section of the ticket. This is
        what will serve to "update" the ticket if details have changed
        since it was last loaded.
        """
        try:
            data = DB.execute('load_ticket', ticket_id=self.ticket_id)[0]
        except IndexError as error:
            raise NoTicketFound from error

        header_data = [
            self.ticket_id,
            data['ticket_title'],
            self.assignment_statement(data['project_title']),
            self.assignment_statement(
                data['resolution_title'],
                data['timestamp_resolution_assigned']
            ),
            data['timestamp_created'].date().isoformat()
        ]

        key_column_width = max([len(x) for x in self.keys]) + 1
        header_rows = [
            f'{key.ljust(key_column_width)}{data}'
            for key, data in zip(self.keys, header_data)
        ]

        # Append the bar.
        header_rows.append('-'*10)
        return header_rows

    def rebuild(self):
        existing_body = self.fetch_existing_ticket_body()
        header_rows = self.create_header()
        ticket_rows = header_rows + existing_body

        with open(self.ticket_filepath, 'w') as ticketfile:
            ticketfile.write('\n'.join(ticket_rows))

    def open(self):
        self.rebuild()
        Popen([os.environ['NOTEPAD'], self.ticket_filepath])
