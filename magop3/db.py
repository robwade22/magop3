import os
from typing import List, Dict

import psycopg2


class DB:
    """All interactions with the database handled here."""
    @classmethod
    def execute(cls, label, **kwargs):
        """Execute the SQL query stored under <label> in this class.

        The queries are stored as strings to be formatted, so the required
        formatting kwargs must also be passed as arguments here.
        """
        stmt = getattr(cls, label).format(**kwargs)
        return cls.execute_sql(stmt)

    @staticmethod
    def execute_sql(stmt: str) -> List[Dict]:
        """Execute an SQL stmt, returning the records as a list of dictionaries."""
        conn = None
        error = None
        records = []
        try:
            conn = psycopg2.connect(**{'host': os.environ.get('DB_HOST'),
                                       'database': os.environ.get('DB_DATABASE'),
                                       'user': os.environ.get('DB_USER'),
                                       'password': os.environ.get('DB_PASSWORD')})

            with conn.cursor() as cur:
                cur.execute(stmt)
                try:
                    data = cur.fetchall()
                    column_names = [col.name for col in cur.description]
                    records = [{col: d[ix]
                                for ix, col in enumerate(column_names)}
                                for d in data]
                except psycopg2.ProgrammingError:
                    pass

        except psycopg2.DatabaseError as e:
            error = e
        finally:
            if conn is not None:
                conn.commit()
                conn.close()
            if error:
                raise error

            return records

    @staticmethod
    def project_resolution_filter(project_code: list, resolution_code: list):
        """Create the statement required to enact the project and resolution
        filtering specified.

        Both <project_code> and <resolution_code> could be None, a list of
        length 1 or a list of length more than 1. Each case requires
        different action here.
        """

        def filter_clause(value_list):
            if len(value_list) == 1:
                # Equality clause on the only element of the list.
                clause = f'= {value_list[0]!r}'
            else:
                # Proper IN clause.
                clause = f'IN {tuple(value_list)}'
            return clause

        filter_clause_list = []
        if project_code:
            filter_clause_list.append(
                f'project_code {filter_clause(project_code)}'
            )
        if resolution_code:
            filter_clause_list.append(
                f'resolution_code {filter_clause(resolution_code)}'
            )

        if not filter_clause_list:
            # No filtering desired.
            return ''

        # The first clause must be prefixed by WHERE.
        filter_clause_list[0] = 'WHERE ' + filter_clause_list[0]
        return ' AND '.join(filter_clause_list)

    new_ticket = """
        INSERT INTO ticket.tickets (title)
        VALUES ({title!r}) 
        RETURNING *
    """

    new_project = """
        INSERT INTO project.projects (project_code, title)
        VALUES ({project_code!r}, {title!r})
        RETURNING *
    """

    history_assign = """
        INSERT INTO {entity_type}.assignment_history 
                    (ticket_id, {entity_type}_code)
        VALUES ({ticket_id!r}, {entity_code!r})
    """

    current_assign = """
        INSERT INTO {entity_type}.assignment (ticket_id, {entity_type}_code)
        VALUES ({ticket_id!r}, {entity_code!r})
        ON CONFLICT ON CONSTRAINT assignment_ticket_id_key
        DO UPDATE SET ticket_id = EXCLUDED.ticket_id,
                      {entity_type}_code = EXCLUDED.{entity_type}_code,
                      timestamp_assigned = NOW()
    """

    alter_ticket = """
        UPDATE ticket.tickets
           SET {attribute_type} = {attribute_value!r}
         WHERE ticket_id = {ticket_id!r}
    """

    alter_project = """
        UPDATE project.projects
           SET {attribute_type} = {attribute_value!r}
         WHERE project_code = {project_code!r}
    """

    # Load the data to print tickets to the terminal. Rename columns as
    # they're selected for presentation purposes.
    print_tickets = """
        SELECT t.ticket_id, 
               t.title AS ticket, 
               t.timestamp_created AS created,
               p.title AS project,
               r.title AS resolution
          FROM ticket.tickets t
               LEFT JOIN project.assignment pa
                   USING (ticket_id)
               LEFT JOIN resolution.assignment ra
                   USING (ticket_id)
               LEFT JOIN project.projects p
                   USING (project_code)
               LEFT JOIN resolution.resolutions r
                   USING (resolution_code)
               {project_resolution_filter}
         ORDER BY ra.resolution_code
    """

    print_projects = """
        SELECT p.project_code, p.title,
               tc.ticket_count
          FROM project.projects p
               LEFT JOIN (
                   SELECT project_code, COUNT(ticket_id) AS ticket_count
                     FROM project.assignment
                    GROUP BY project_code
               ) tc
               USING (project_code)
    """

    print_resolutions = """
        SELECT r.resolution_code, r.title,
               tc.ticket_count 
          FROM resolution.resolutions r
               LEFT JOIN (
                   SELECT resolution_code, COUNT(ticket_id) as ticket_count
                     FROM resolution.assignment
                    GROUP BY resolution_code
               ) tc
               USING (resolution_code)
    """

    load_ticket = """
        SELECT t.ticket_id,
               t.title AS ticket_title,
               t.timestamp_created,
               p.title AS project_title,
               r.title AS resolution_title,
               ra.timestamp_assigned AS timestamp_resolution_assigned
          FROM ticket.tickets t
               LEFT JOIN project.assignment pa
                   USING (ticket_id)
               LEFT JOIN resolution.assignment ra
                   USING (ticket_id)
               LEFT JOIN project.projects p
                   USING (project_code)
               LEFT JOIN resolution.resolutions r
                   USING (resolution_code)
               WHERE ticket_id = {ticket_id}
    """

    # This is used by delete to understand the tickets it's dealing with.
    get_tickets = """
        SELECT t.ticket_id, 
               p.title AS project,
               r.title AS resolution
          FROM ticket.tickets t
               LEFT JOIN project.assignment pa
                   USING (ticket_id)
               LEFT JOIN resolution.assignment ra
                   USING (ticket_id)
               LEFT JOIN project.projects p
                   USING (project_code)
               LEFT JOIN resolution.resolutions r
                   USING (resolution_code)
               {project_resolution_filter}
         ORDER BY ra.resolution_code
    """

    delete_ticket = """
        DELETE FROM {table}
         WHERE ticket_id = {ticket_id!r}
    """

    delete_project = """
        DELETE FROM project.projects
         WHERE project_code = {project_code!r}
    """
