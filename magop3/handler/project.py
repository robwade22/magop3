"""
Contains the handler for Project actions.
"""

import pandas as pd

from magop3.db import DB
from magop3.display.terminal_data_frame import TerminalDataFrame
from magop3.handler import TicketHandler


class ProjectHandler:
    """Handle and enact commands around Project entities."""
    @classmethod
    def new(cls, project_code: str, title: str, **kwargs):
        """
        Create a new project with <project_code> and <title>.
        """
        assert len(project_code) == 3, 'Project code must be 3 characters!'
        cls._new(project_code, title)

    @classmethod
    def alter(cls, project_code: str, title: str, **kwargs):
        """
        Alter the project with <project_code>.
        """
        if title:
            cls._alter(project_code, 'title', title)

    @classmethod
    def print_(cls, **kwargs):
        """
        Print a summary of projects to the terminal.
        """
        prj = pd.DataFrame(DB.execute('print_projects'))
        prj['ticket_count'] = prj.ticket_count.fillna(0).astype(int)
        prj = prj.astype(str).replace('None', '')
        TerminalDataFrame(prj).publish()

    @classmethod
    def delete(cls, project_code: str, **kwargs):
        """
        Delete the project with <project_code> - and all tickets within.
        """
        print(f'Deleting project {project_code}...')

        # Most of this is simply a ticket delete - the confirmation of
        # deletion we want is contained within this.
        TicketHandler.delete(project_code=project_code)

        # The deletion of the tickets should have handled the deletion
        # of all tickets PLUS all references in the project assignment
        # table.
        cls._delete(project_code)
        print('Complete!')

    @staticmethod
    def _new(project_code, title):
        DB.execute('new_project', project_code=project_code, title=title)
        print(f'\nNew project created with title {title!r} and project_code '
              f'{project_code!r}\n')

    @staticmethod
    def _alter(project_code, attribute_type, attribute_value):
        DB.execute('alter_project', project_code=project_code,
                   attribute_type=attribute_type,
                   attribute_value=attribute_value)

    @staticmethod
    def _delete(project_code):
        DB.execute('delete_project', project_code=project_code)
