import pandas as pd

from magop3.db import DB
from magop3.display.terminal_data_frame import TerminalDataFrame


class ResolutionHandler:
    """A handler for all action commands on a resolution entity."""
    @classmethod
    def print_(cls, **kwargs):
        df = pd.DataFrame(DB.execute('print_resolutions'))
        df['ticket_count'] = df.ticket_count.fillna(0).astype(int)
        df = df.astype(str).replace('None', '')
        TerminalDataFrame(df).publish()
