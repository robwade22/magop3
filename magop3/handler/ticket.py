import os

import pandas as pd

from magop3.db import DB
from magop3.display.terminal_data_frame import TerminalDataFrame
from magop3.display.ticket_builder import TicketBuilder, NoTicketFound


class TicketHandler:
    """A handler for all action commands on a ticket entity."""
    @classmethod
    def new(cls, title, project_code, resolution_code, load, **kwargs):
        """Command issued to create a new ticket with <title>. Assign it to the
        specified <project_code> and <resolution_code> and load if required."""
        ticket_id = cls._new(title=title)
        cls.assign(ticket_id, project_code, resolution_code, load)

    @classmethod
    def assign(cls, ticket_id, project_code, resolution_code, load, **kwargs):
        """Command issued to assign ticket <ticket_id> to <project_code> and
        <resolution_code>, and <load> the ticket if required."""
        if project_code:
            cls._assign(ticket_id, 'project', project_code)
        if resolution_code:
            cls._assign(ticket_id, 'resolution', resolution_code)
        if load:
            cls._load(ticket_id)

    @classmethod
    def alter(cls, ticket_id, title, load, **kwargs):
        """Command issued to alter an attribute of <ticket_id>."""
        if title:
            cls._alter(ticket_id, 'title', title)
        if load:
            cls._load(ticket_id)

    @classmethod
    def load(cls, ticket_id: list, **kwargs):
        """Command issued to load tickets."""
        for load_id in ticket_id:
            cls._load(load_id)

    @classmethod
    def print_(cls, project_code: list, resolution_code: list, **kwargs):
        """Command issued to print a summary of tickets to the terminal."""
        # Build the filter to be enacted on the tickets printed.
        pr_filter = DB.project_resolution_filter(project_code, resolution_code)

        # Create the DataFrame of data to be printed. For display purposes
        # we use just the date of the ticket timestamp_created and stringify
        # the whole df.
        df = pd.DataFrame(
            DB.execute('print_tickets', project_resolution_filter=pr_filter)
        )
        if not df.empty:
            df['created'] = df.created.dt.date
            df = df.astype(str).replace('None', '')

        TerminalDataFrame(df).publish()

    @classmethod
    def delete(cls, ticket_id: int = None, project_code: str = None,
               resolution_code: str = None, **kwargs):
        """
        Delete tickets that match the passed criteria.
        """

        # argparse doesn't allow you to enforce *specify at least one
        # of* for a group of arguments - we need to enforce that here
        # for the filtering arguments otherwise it'll try to delete all.
        if all([x is None for x in [ticket_id, project_code, resolution_code]]):
            print('\nAt least one of ticket_id, project_code or '
                  'resolution_code must be specified')
            return

        if ticket_id is not None:
            # We know we're only deleting a single ticket - and can get
            # straight to it.
            cls._delete(ticket_id, confirm=True)
            return

        # DB.project_resolution_filter requires a list or None, so enact
        # that here.
        project_code = [project_code] if project_code is not None else None
        resolution_code = [resolution_code] if resolution_code is not None else None

        # Build the filter to get the tickets selected.
        prf = DB.project_resolution_filter(project_code, resolution_code)
        tickets = pd.DataFrame(
            DB.execute('get_tickets', project_resolution_filter=prf)
        )

        if tickets.empty:
            print('No tickets found to delete!')
            return

        # Want to print a nice summary of what is to be deleted.
        summary = (tickets.groupby(['project', 'resolution'])
                          .size()
                          .to_frame(name='ticket_count'))

        print('\nDELETE SUMMARY:')
        print(summary)

        print('\nmagop3 is about to delete these tickets permanently')
        validated = input('Proceed? (Y/n) > ') in ('', 'Y')
        if not validated:
            return
        print('Actioning delete...')

        tickets.ticket_id.apply(cls._delete)

    @staticmethod
    def _new(title=None):
        title = '' if title is None else title
        response = DB.execute('new_ticket', title=title)
        return response[0]['ticket_id']

    @staticmethod
    def _assign(ticket_id, entity_type, entity_code):
        assign_kwargs = {
            'entity_type': entity_type,
            'ticket_id': ticket_id,
            'entity_code': entity_code
        }

        # Log the assignment in the assignment history.
        DB.execute('history_assign', **assign_kwargs)
        # Upsert the current assignment.
        DB.execute('current_assign', **assign_kwargs)

    @staticmethod
    def _alter(ticket_id, attribute_type, attribute_value):
        DB.execute(
            'alter_ticket',
            ticket_id=ticket_id,
            attribute_type=attribute_type,
            attribute_value=attribute_value
        )

    @staticmethod
    def _load(ticket_id):
        try:
            TicketBuilder(ticket_id).open()
        except NoTicketFound:
            print(f'\nNo ticket found with ticket_id {ticket_id}!\n')

    @staticmethod
    def _delete(ticket_id, confirm=False):
        if confirm:
            print(f'magop3 is about to delete ticket {ticket_id} permanently')
            validated = input('Proceed? (Y/n) > ') in ('', 'Y')
            if not validated:
                return

        for table in ['project.assignment', 'project.assignment_history',
                      'resolution.assignment', 'resolution.assignment_history',
                      'ticket.tickets']:
            DB.execute('delete_ticket', ticket_id=ticket_id, table=table)

        filename = f'T{ticket_id}.txt'
        filepath = os.path.join(os.environ['VAULTS'], filename)
        os.remove(filepath)
