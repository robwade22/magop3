/*
The database tables that serve the system.

Three entity tables, then two sets of assignment tables.

One set of assignment tables will hold the current assignment only for speed
purposes, and be overwritten when there's a new assignment per ticket_id. The
other will store all assignments in history for a ticket_id.
*/


BEGIN;

CREATE SCHEMA IF NOT EXISTS resolution;
CREATE SCHEMA IF NOT EXISTS project;
CREATE SCHEMA IF NOT EXISTS ticket;

CREATE TABLE IF NOT EXISTS resolution.resolutions (
    PRIMARY KEY(resolution_code),
    resolution_code VARCHAR(3) NOT NULL,
    title           VARCHAR    NOT NULL,
    UNIQUE(resolution_code)
);

CREATE TABLE IF NOT EXISTS project.projects (
    PRIMARY KEY(project_code),
    project_code VARCHAR(3) NOT NULL,
    title        VARCHAR    NOT NULL,
    UNIQUE(project_code)
);

CREATE TABLE IF NOT EXISTS ticket.tickets (
    PRIMARY KEY(ticket_id),
    ticket_id         SERIAL,
    title             VARCHAR   NOT NULL,
    timestamp_created TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS resolution.assignment_history (
    PRIMARY KEY(ticket_id),
    ticket_id          INTEGER    NOT NULL,
    resolution_code    VARCHAR(3) NOT NULL,
    timestamp_assigned TIMESTAMP  DEFAULT NOW(),
    FOREIGN KEY(ticket_id)       REFERENCES ticket.tickets(ticket_id),
    FOREIGN KEY(resolution_code) REFERENCES resolution.resolutions(resolution_code)
);

CREATE TABLE IF NOT EXISTS project.assignment_history (
    PRIMARY KEY(ticket_id),
    ticket_id          INTEGER    NOT NULL,
    project_code       VARCHAR(3) NOT NULL,
    timestamp_assigned TIMESTAMP  DEFAULT NOW(),
    FOREIGN KEY(ticket_id)    REFERENCES ticket.tickets(ticket_id),
    FOREIGN KEY(project_code) REFERENCES project.projects(project_code)
);

CREATE TABLE IF NOT EXISTS resolution.assignment (
    PRIMARY KEY(ticket_id),
    ticket_id          INTEGER    NOT NULL,
    resolution_code    VARCHAR(3) NOT NULL,
    timestamp_assigned TIMESTAMP  DEFAULT NOW(),
    UNIQUE(ticket_id),
    FOREIGN KEY(ticket_id)       REFERENCES ticket.tickets(ticket_id),
    FOREIGN KEY(resolution_code) REFERENCES resolution.resolutions(resolution_code)
);

CREATE TABLE IF NOT EXISTS project.assignment (
    PRIMARY KEY(ticket_id),
    ticket_id          INTEGER    NOT NULL,
    project_code       VARCHAR(3) NOT NULL,
    timestamp_assigned TIMESTAMP  DEFAULT NOW(),
    UNIQUE(ticket_id),
    FOREIGN KEY(ticket_id)    REFERENCES ticket.tickets(ticket_id),
    FOREIGN KEY(project_code) REFERENCES project.projects(project_code)
);

COMMIT;
