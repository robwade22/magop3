/*
Populate the resolution table with the recommended resolutions
*/

BEGIN;

INSERT INTO resolution.resolutions (resolution_code, title)
VALUES 
    ('inp', 'In Progress'),
    ('tdy', 'Today'),
    ('thw', 'This Week'),
    ('nxw', 'Next Week'),
    ('nxm', 'Next Month'),
    ('nxq', 'Next Quarter'),
    ('nxy', 'Next Year'),
    ('dcm', 'Decommissioned'),
    ('cmp', 'Complete')
;

COMMIT;
